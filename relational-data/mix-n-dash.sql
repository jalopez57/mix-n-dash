DROP TABLE IF EXISTS bartenders;
DROP TABLE IF EXISTS hosts;
DROP TABLE IF EXISTS events;


CREATE TABLE users (
    id SERIAL NOT NULL UNIQUE,
    first TEXT NOT NULL,
    last TEXT NOT NULL,
    phone_number TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    state TEXT NOT NULL,
    city TEXT NOT NULL
);

CREATE TABLE bartenders (
    id SERIAL NOT NULL UNIQUE,
    about TEXT NOT NULL,
    drinks TEXT [] NOT NULL,
    user_id INTEGER NOT NULL UNIQUE REFERENCES users("id") ON DELETE CASCADE
);

CREATE TABLE events (
    id SERIAL NOT NULL UNIQUE,
    address TEXT NOT NULL,
    city TEXT NOT NULL,
    state TEXT NOT NULL,
    date DATE NOT NULL,
    time TIME NOT NULL,
    user_id INTEGER NOT NULL REFERENCES users("id") ON DELETE CASCADE,
    bartender_id INTEGER NOT NULL REFERENCES bartenders("id") ON DELETE CASCADE
);

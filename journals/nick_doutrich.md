11/14/22:
    - In our stand up we decided the best use of time today would be to go through and start to build out our front end a little bit, as we learn more about our new backend softwares
    - We integrated a nice template from bootstrap to give the landing page some flair.
    - We started to organize the directories in our ghi directory so when it's time to create components we already have an idea of how they're going to look.
    - Goals for tomorrow:
        - Start to learn more about FastAPIs and MongoDB
        - Get Nav component up
        - Start on popout form components on using React
11/15/22:
    - Questions for Dalonte tomorrow:
        - Do we need to seperate out Bartenders for accounts/public view? Is there a way to make that account info public, just parts of it, to display in a detail page for users;
        - Authentication questions, talk through with team before Dalonte;
        - Don't give us answers if he feels like we will naturally come across the solution to what we need
    - Worked on figuring out how to setup PostgreSQL and started organizing directories for FastAPI endpoints
    - Blockers are concerns with using authentication, need to review more
    - When account is created, makes request to AccountIn with info required for account, and also makes request for BartenderIn with info required for that, but all from one form, same with Hosts??
11/16/22
    - Clean up hosts -> users
    - Make sure bartender routes and queries are updated with new DB properties
    - Start working on authentication with users being the properties
11/20/22
    - Finished up backend, with all endpoints auth protected that need to be
11/22/22
    - Worked on authorizing the front end
    - Setup login/logout/account detail pages
    - Figure out how to store access token and call it across app
11/28/22
    - Finally had blocked break through and got token stored using redux instead of localStorage
    - Just had to add credentials...
11/29/22
    - Started working on Account Detail page, pulling all the data to this page
    - Tricky because of how React/Redux loads data, trying to load from 3 separate tables in the same page,
    - These tables are dependant on each other, found cases for using '?' to help with rendering
11/30/22
    - Worked on same implementations of accessing data, created Modals for all the forms
    - Created dynamic Bartender detail pages, and a list page
12/01/22
    - Had weird blocked where data from database was incorrectly being inserted into rows because of JOIN issues
    - Fixed using a helper function to make a call to another table
    - All functional things working mostly with website, implemented a way to access the external API with the selector of drinks for Bartender creations
12/05/22 - 12/06/22
    - Worked on small issues with features
    - Changed cities to lower case automatically in DB
    - Added a function that would convert the display of cities to upper case
    - Worked with group on their blockers
12/07/22
    - Helped transfer all functionality to the account dashboard page
    - Taught back to team what some of the functionality was doing so they understood better
    - Finished getting dashboard ready
12/08/22
    - Cleaned up code
    - Organized directories
    - Worked on windows vs mac running from fresh clone
12/09/22
    - Began and finished getting website deployed
    - Created CI/CD file
    - Setup group with projects on Gitlab
    - Setup render/git/finished deployment

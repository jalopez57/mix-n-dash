11/14:
We need to figure out work allocation, and that will become more clear once we compile the list of tasks/features/components that need to be implemented for the webpage. At the moment, I want to learn MongoDB and take the lead on the backend content of the project. Currently, it makes sense to me if we work on the front end as we wait on lectures and additional learnings. Today, I created my own branch, and we will be working within our own branches (the four members of the team = four branches). At the end, we will merge into one main branch.

11/15:
Brainstormed on the approach to databases and relations between tables. Decided on using FastAPI in combo with PostgreSQL. Ran into issue with docker build, since it is saying docker file cannot be empty. Turns out it was the location of the dockerfile and how we were pointing to it, so that is now resolved.

11/16:
Working to get one api up and running successfully, so that can have a portion of the project complete and can be used as reference moving forward. Right now, the idea I have in mind is leveraging the strengths of each group member, all while everyone has visibility and exposure of the entire code holistically.
Decision made: singular approach. One bartender = one city. Each event will have one host and one bartender tied to it.
Microservices environment: front-end and back-end.
Things to do: need to get database secured, including avatar
Today I completed the bartenders file for both queries and routers.

11/17:
We are pivoting and having one general user. Upon creating the user account, there will be an option to 'become a bartender'. In other words, not every user will be a bartender, but every bartender is a user.
We need to make a restriction where one user can only become one bartender.

11/18:
adding the organization table to the tracker.
begin to shell out the front end, even if its just static for now.
finishing up the back end to make sure bartenders, users, and events all have the right connections and calls.

11/21:
Right now, as long as the user is authenticated, they can view/edit/update any accounts. We need to implement a way to make it so that people can only edit/ update/ delete their own account, no other accounts. Per conversation with Dalonte, we can take two approaches: we can define roles and make clear the authorizations and capabilities, or we can create another conditional logic. Goals today: finalize forms on front end, quality check on back end.

11/22:
implemented the proper restrictions and authentication/auth logic to ensure that only the user who is logged in can make changes just to their account, not anyone else's account.


11/23:
tweaked some minor details for the front end forms and pop ups.


11/28:
the following are items on the to-do list–
front: update the css
back: mostly done, just need the front end to link up and behave accordingly with the back end
Overall: we are at a state where the back end and front end need to be linked up and function per the requirements and design
Also need to incorporate the external Drinks API

11/29:
researched and implemented a simple checkbox component for the purpose of create bartender and edit bartender details. The goal is to pull the top 20 drinks from the external API and enable users to select from that list to build their profile.
Features and components left: component to display all bartenders, small card with name and location,

11/30:
Began and completed the bartenderslist file, which is the code necessary to list all bartenders that are stored in the database. Essentially, we want this to be available to the public without the need for log in. This will include the name and email of each bartender, and the public can click an individual name to see the bartender detail page.
We also decided to cancel the idea of a checkbox for bartender create page, and we are instead going to implement a drop down list.

12/1:
need to make some styling updates, especially just to organize components and the overall output of the webpage.
figured out the npm install requirements needed to load the project and address any errors.

12/2:
Created comprehensive bullet points for items on the to-do list between now and due date.
Walked through the website to update some visuals and design components/front end.

12/5:
recap of what is done and what is left.
Compiled all of the stretch goals we have thought of so far, which can be added to the README file at a later time.
Wrote up information to include on the homepage that introduces the platform.

12/6:
Began and completed the majority of the README file. Still remaining on there is the TEST portion, since we are not completely done with the four tests needed.

12/7:
Revised the CSS and other styling elements to be more consistent throughout the different pages on the website (width of boxes and buttons, colors, etc.)

12/8:
Deleted repo so I can do a fresh clone fresh pull and basically follow the README instructions to make sure everything works as intended.
Performing quality check on console, too.
Updated yaml, updated tests, and updated README.

12/9:
brainstorm methods and ways to incorporate a "are you of legal drinking age?" button for the users to select Yes or No once they enter the website.
Decided to make this a stretch goal.

12/12: final walk through of functionality. rough draft/initial presentation to Dalonte. Finalized the README file.

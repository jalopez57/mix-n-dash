November 14, 2022: Today we fleshed out the the wireframe for our project and it appears we are on the right track.

November 15,2022: Today studied FastAPI and MongoDB, also we decided to use PostgreSQL because that would be best suited for what we are trying to create.
November 16,2022: came up with the name of the service: we decided on the name Mix’n Dash.
We also came to the conclusion that Cristoval and I would be more focused on the frontend of this project.

November 17,2022 : Today i created a logo for the our project. Came out fairly well , also have the color scheme of the site down for the most part. Also learned more about hooks to better prepare the frontend.

November 18, 2022:
Cristoval and I spent the day researching and planning out the design of the entire website. So far, it’s going well, but we’ve encountered some compatibility issues with the templates we’ve been looking at because they’re written in a different language and would be difficult to translate. Came across some interesting potential UI inspiration.
Found some interesting stuff on https://elements.envato.com/web-templates/site-templates/react

November 21, 2022: Kept looking at templates for inspiration and came across a theme that we liked, attempted to use but realized this was not going to work after about 5 hours of trying to make it work.

November 22, 2022: We continued trying to come with a solution for using a theme for the ux/ui of the frontend. Unfortunately no luck, we came across

November 23, 2022: We were able to create a Calendar component using MUI and it was so exciting to finally really make it work. Decided to get a better understanding on how these libraries work so started to watch some videos on mui, bootstrap and tailwind to have a better understanding on how to approach the problems we have.

November 28, 2022: Ran into some serious docker issues this day. Had to basically scrap everything and start from scratch. Was able to finally fix it. Found out we arent going to be using the calendar component. Focused alot of the day on React hooks since Cristoval and I will be more focused on the frontend.

November 29,2022: Pair programmed with Cristoval regarding the main page and nav bar.

December 1, 2022:
Was able to finish the input forms, this will later be used for our login forms and other components later on.

December 2,2022:
Did some research on how to go about the detail pages for the users and bartenders. Came across a couple good things. Really liked MDbootstrap but it seems difficult to implement but proceeded to attempting to do anyways.

December 3, 2022: Worked with Cristoval to try to better implement the wireframe that we originally thought of. Here we decided to make some design decisions to implement moving forward.

December 5,2022: was able to fully finish what our detail pages would look like after installing MDBootstrap and using those components to get a better UI/UX experience for our detail pages.

December 6, 2022: continued to tweak the user detail page that the rest of the detail pages would be based off of. Received feedback from Nick and Ray regarding the progress Cristoval and I have made on the frontend.

December 7,2022: Did pair programming with Cristoval to better finish up some of the CSS and really focus on what the issues were with the bartender account info.

December 8, 2022: Was pair programming with Cristoval and Nick for a bit, then did some finishing touches with the frontend. Got the tests working.

Dec 12, 2022: Today we are turning in the project but Nick had found a weird bug that wouldn’t let you login while in incognito mode or on mobile. We were able to discover what the issue was. Currently a more permanent solution does not seem likely due to safari, incognito blocking cookies from third parties which is a crucial component of logging in.

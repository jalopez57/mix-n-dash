### GET a list of drinks

* Endpoint path: /drinks
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: A list of drinks
* Response shape:
    ```json
    {
      "drinks": [
        {
          strDrink:"Mojito",
          strDrinkThumb:"https://www.thecocktaildb.com/images/media/drink/3z6xdi1589574603.jpg",
          ingredients: string,



        }
      ]
    }


### GET a list of  custom drinks

* Endpoint path: /drinks/custom
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: A list of custom drinks
* Response shape:
    ```json
    {
      "custom_drinks": [
        {
          drink_name:"shirley-temple",
          drink_image:"bartender-uploaded",
          ingredients: string,
          bartender_id: id,

        }
      ]
    }



### CREATE  custom drink


* Endpoint path: /drinks/custom
* Endpoint method: POST

* Headers:
  * Authorization: Bearer token

* Request shape (form):
* drink_name:"shirley-temple",
* drink_image:"bartender-uploaded",
* ingredients: string,
* bartender_id: id,


* Response: Create custom drink
* Response shape (JSON):
    ```json
  {
      "custom_drinks": [
        {
          drink_name:"shirley-temple",
          drink_image:"bartender-uploaded",
          ingredients: string,
          bartender_id: id,

        }
      ]
    }






### DELETE a drink

* Endpoint path: /drinks/custom/id
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token


* Response: Deleted custom drink
* Response shape (JSON):
    ```json
   {
	"deleted": boolean }





### UPDATE a drink

* Endpoint path: /drinks/custom/id
* Endpoint method: PUT

* Headers:
  * Authorization: Bearer token

* Request shape (form):
* drink_name:"shirley-temple",
* drink_image:"bartender-uploaded",
* ingredients: string,
* bartender_id: id,


* Response: Update custom drink
* Response shape (JSON):
    ```json
  {
      "custom_drinks": [
        {
          drink_name:"shirley-temple",
          drink_image:"bartender-uploaded",
          ingredients: string,
          bartender_id: id,

        }
      ]
    }

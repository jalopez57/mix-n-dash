### Log in

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
        «key»: type»,
      },
      "token": string
    }
    ```


### Log out

* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true


### CREATE  new event host


* Endpoint path: /accounts
* Endpoint method: POST


* Headers:
  * Authorization:

* Request shape (form):

* "name": string,
* "about": string,
* "image_url": string,
* "drinks": string,
* "phone_number": integer,
* "email": email,
* "state": string,
* city: string,


* Response: Create a new bartender
* Response shape (JSON):
    ```json
    {
      "bartenders": [
        {
          "name": string,
          "about": string,
          "image_url": string,
          "phone_number": integer,
          "email": email,
          "state": string,
          "city": string,

        }
      ]
    }

### DETAIL account


* Endpoint path: /accounts/id
* Endpoint method: GET


* Headers:
  * Authorization: Bearer token


* Response: Create a new bartender
* Response shape (JSON):
    ```json
    {
      "account": [
        {
          "name": string,
          "about": string,
          "image_url": string,
          "phone_number": integer,
          "email": email,
          "state": string,
          "city": string,

        }
      ],

        "bookings":[
    {
     "bartender_name": string,
     "address":string,
     "date/time":string,

        }
    }

### CREATE booking


* Endpoint path: /bookings
* Endpoint method: POST


* Headers:
  * Authorization:

* Request shape (form):

* bartender_name: string,
* host_name: string,
* address: string,
* date/time: string,


* Response: Create a booking
* Response shape (JSON):
    ```json
    {
      "bookings": [
        {
          "bartender_name": string,
          "host_name": string,
          "address":string,
          "date/time": string,
        }
      ]
    }


### GET list of  bookings


* Endpoint path: /bookings
* Endpoint method: GET
* Query parameters:
    * q: bartenders, host name


* Headers:
  * Authorization:


* Response: list of bookings
* Response shape (JSON):
    ```json
    {
      "bookings": [
        {
          "bartender_name": string,
          "host_name": string,
          "address":string,
          "date/time": string,
        }
      ]
    }

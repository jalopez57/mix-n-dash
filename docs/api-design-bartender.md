### GET a list of bartenders

* Endpoint path: /bartenders
* Endpoint method: GET
* Query parameters:
  * q: city, drinks, date,

* Response: A list of Bartenders
* Response shape:
    ```json
    {
      "bartenders": [
        {
          "name": string,
          "description": string,
          "image_url": string,
          "drinks": string,
        }
      ]
    }

### CREATE  new bartender


* Endpoint path: /bartenders
* Endpoint method: POST


* Headers:
  * Authorization:

* Request shape (form):

* "name": string,
* "about": string,
* "image_url": string,
* "drinks": string,
* "phone_number": integer,
* "email": email,
* "state": string,
* city: string,


* Response: Create a new bartender
* Response shape (JSON):
    ```json
    {
      "bartenders": [
        {
          "name": string,
          "about": string,
          "image_url": string,
          "drinks": string,
          "phone_number": integer,
          "email": email,
          "state": string,
          "city": string,

        }
      ]
    }





### DELETE a bartender

* Endpoint path: bartenders/id/
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token


* Response: Delete a bartender
* Response shape (JSON):
    ```json
    {
        "deleted":false
    }

### UPDATE a bartender

* Endpoint path: /bartenders/id
* Endpoint method: PUT


* Headers:
  * Authorization: Bearer token

* Request shape (form):

* "name": string,
* "about": string,
* "image_url": string,
* "drinks": string,
* "phone_number": integer,
* "email": email,
* "state": string,
* city: string,

* Response: updated bartender
* Response shape (JSON):
    ```json
    {
      "bartenders": [
        {
          "name": string,
          "about": string,
          "image_url": string,
          "drinks": string,
          "phone_number": integer,
          "email": email,
          "state": string,
          "city": string,

        }
      ]
    }


### DETAIL bartender


* Endpoint path: /bartenders/id
* Endpoint method: GET


* Headers:
  * Authorization: Bearer token


* Response: Create a new bartender
* Response shape (JSON):
    ```json
     {
      "bartenders": [
        {
          "name": string,
          "about": string,
          "image_url": string,
          "drinks": string,
          "phone_number": integer,
          "email": email,
          "state": string,
          "city": string,

        }
      ],

     "reviews":[
    {
     "name": string,
     "rating": integer,
     "comments":string,

        }
     ],

        "bookings":[
    {
     "host_name": string,
     "address":string,
     "date/time":string,

        }
        ]
    }

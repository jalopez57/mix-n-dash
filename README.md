## Mix n Dash

## Demo of Deployed Site

https://mix-n-dash.gitlab.io/mix-n-dash

## Project Team

[Nick Doutrich](./journals/nick_doutrich.md)
[Ray Khan](./journals/ray_khan.md)
[Jose Lopez](./journals/jose_lopez.md)
[Cristoval Ramirez](./journals/cristoval_ramirez.md)

## Design

[Design Docs (including API design and WIREFRAME layout)](./docs)
[Design for User Accounts](./docs/api-design-accounts.md)
[Design for Bartenders](./docs/api-design-bartender.md)
[WireFrame](./docs/projectpng.png)

Initially, we constructed the overall design and flow of the homepage (what the visitor sees first when they go to the webpage), and the intent was to convey the purpose of the platform. The navigation bar enables the visitor to: login if they already have an account, sign up for an account, or view the roster of bartenders. The home page also establishes the overall design and flow of the webpage.
The other two main pages for our project are the account detail page and the bartender detail page. The account page, which requires authentication, serves as a dashboard for account management where the user (after logging in) is able to see upcoming events as well as details attached to their profile. They are also able to ‘become a bartender’ if they wish to do so.
The bartender detail page, which can be seen without being logged in, includes key information about the particular individual you select, such as the bartender’s about me section, location, and the drinks they are capable of making.
We chose to incorporate the use of modals on our site for all of the other forms, such as the login, account sign up, bartender sign up, and booking a bartender for an upcoming event.

## Intended market

Our platform is able to connect event hosts and bartenders. For those who are hosting an upcoming event for a variety of occasions (birthday, wedding, etc.), they are able to go on the website, create an account, and book a bartender that meets their needs. Additionally, an individual can create an account and choose to become a bartender if they wish to offer bartending services.
​
## Functionality

With no login, a visitor is able to:

- view the entire roster of bartenders
- see bartender detail page
  - see name/about me/location/drinks for each bartender
- choose to create an account

Once account is created, the user can:

- book a bartender for a future event
- choose to become a bartender on the platform
- select one or more drinks for their bartender profile

## Stretch goals

- pricing component for each bartender (hourly rate, or fixed fee)
  - filter by price range
- go from statewide to multi-state to nationwide
- ability for hosts/bartenders to communicate on the website by sending messages
- Users leave reviews for bartenders, and bartenders leave reviews for users
- Bartenders can create custom drinks and upload an image for the drink
- incorporate external API for financial side (Stripe for example)
- bartender being able to serve multiple cities
- build out a footer
  - include things such as About Us section and Contact Us section
- build out the availability (date and time) for each bartender

## Set up

1. Clone: https://gitlab.com/mix-n-dash/mix-n-dash
2. Create volume with the following command in terminal: docker volume create postgres-data
3. Build the containers and images: docker compose build

- If you have Apple Mac M1, use this command: DOCKER_DEFAULT_PLATFORM=linux/amd64 docker-compose build

4. Start the containers: docker compose up
5. Open up web browser and go to: http://localhost:3000/
   Success!

​

## Tests

- tests/test_user_routes.py / Jose Lopez
- tests/test_bartender_routes.py/test_list_bartenders / Ray Khan
- tests/test_bartender_routes.py/test_create_bartender / Nick Doutrich
- tests/test_events_routes.py / Cristoval Ramirez

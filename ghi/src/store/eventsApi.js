import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { accountsApi } from "./accountsApi";

export const eventsApi = createApi({
  reducerPath: "events",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_MIX_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = accountsApi.endpoints.getToken.select();
      const { data } = selector(getState());
      if (data && data.access_token) {
        headers.set("Authorization", `Bearer ${data.access_token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getEvents: builder.query({
      query: () => "/api/events",
      providesTags: ["EventsList"],
      credentials: "include",
    }),
    getEvent: builder.query({
      query: (eventId) => `/api/events/${eventId}`,
      providesTags: ["Event"],
      credentials: "include",
    }),
    createEvent: builder.mutation({
      query: (data) => ({
        url: "/api/events",
        body: {
          address: data.address,
          city: data.city,
          state: "TX",
          date: data.date,
          time: data.time,
          bartender_id: data.bartender,
        },
        credentials: "include",
        method: "post",
      }),
      invalidatesTags: ["EventsList"],
    }),
    deleteEvent: builder.mutation({
      query: (eventId) => ({
        url: `/api/events/${eventId}`,
        credentials: "include",
        method: "delete",
      }),
      invalidatesTags: (result) => {
        return (result && ["EventsList"]) || [];
      },
    }),
  }),
});

export const {
  useGetEventsQuery,
  useGetEventQuery,
  useCreateEventMutation,
  useDeleteEventMutation,
} = eventsApi;

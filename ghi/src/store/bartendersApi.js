import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { accountsApi } from "./accountsApi";

export const bartendersApi = createApi({
  reducerPath: "bartenders",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_MIX_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = accountsApi.endpoints.getToken.select();
      const { data } = selector(getState());
      if (data && data.access_token) {
        headers.set("Authorization", `Bearer ${data.access_token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getBartenders: builder.query({
      query: () => "/api/bartenders",
      providesTags: ["BartendersList"],
    }),
    getDrinks: builder.query({
      query: () => "/api/drinks",
      providesTags: ["DrinksList"],
    }),
    getBartender: builder.query({
      query: (bartenderId) => `/api/bartenders/${bartenderId}`,
      providesTags: ["Bartender"],
    }),
    updateBartender: builder.mutation({
      query: (data) => ({
        url: "/api/bartender",
        body: {
          about: data.about,
          drinks: data.drinksList,
        },
        credentials: "include",
        method: "put",
      }),
      invalidatesTags: (result) => {
        return (result && ["BartendersList"]) || [];
      },
    }),
    createBartender: builder.mutation({
      query: (data) => ({
        url: "/api/bartenders",
        body: {
          about: data.about,
          drinks: data.drinksList,
        },
        credentials: "include",
        method: "post",
      }),
      invalidatesTags: ["BartendersList"],
    }),
  }),
});

export const {
  useGetBartendersQuery,
  useGetBartenderQuery,
  useUpdateBartenderMutation,
  useCreateBartenderMutation,
  useGetDrinksQuery,
} = bartendersApi;

// ERROR NOTIFICATION
// NAV & MAIN
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./css/index.css";
import Main from "./Main";
import Nav from "./Nav";
import BartenderList from "./bartenderComponents/bartenderList";
import UserDetail from "./accountComponents/accountDetail";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="account/detail" element={<UserDetail />} />
        <Route path="bartenders" element={<BartenderList />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

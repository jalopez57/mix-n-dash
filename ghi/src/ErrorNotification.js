import Alert from 'react-bootstrap/Alert';

function ErrorNotification(props) {
    if (!props.error) {
        return null;
    }

    return (
        <div>
        <Alert key="danger" variant="danger">
          {props.error}
        </Alert>
        </div>
    );
}

export default ErrorNotification;

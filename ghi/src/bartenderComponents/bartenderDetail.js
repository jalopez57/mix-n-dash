// data
import { useGetBartenderQuery } from "../store/bartendersApi";
import { useGetTokenQuery } from "../store/accountsApi";
import { useState, useEffect } from "react";
import BookEventModal from "../accountComponents/eventCreateModal";

// Photos
import Carousel from "react-bootstrap/Carousel";
// import M from "../assets/img/cocktail1.jpg";

// Card styles
import {
  MDBCol,
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
} from "mdb-react-ui-kit";

function BartenderDetail(props) {
  const { data: bartenderData, isLoading } = useGetBartenderQuery(props.id);
  //   const [drinksImages, setImages] = useState([]);
  const [drinksObjArray, setDrinksObj] = useState([]);
  const { data: tokenData } = useGetTokenQuery();
  async function loadDrinksPictures() {
    let drinksUrls = [];
    let drinksObjArray = [];
    for (let drink of bartenderData.drinks) {
      const response = await fetch(
        `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${drink}`
      );
      const json = await response.json();
      const picture = json.drinks[0].strDrinkThumb;
      drinksUrls.push(picture);
    }
    const drinkNames = bartenderData.drinks;
    const drinkPics = drinksUrls;
    drinkNames.forEach((element, index) => {
      let obj = {};
      obj["name"] = element;
      obj["image"] = drinkPics[index];
      drinksObjArray.push(obj);
    });
    setDrinksObj(drinksObjArray);
  }

  function upperFirstChar(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  useEffect(() => {
    if (bartenderData) {
      loadDrinksPictures();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bartenderData]);

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  let signupClass = "d-none d-flex justify-content-center mb-2";
  let eventClass = "d-flex justify-content-center mb-2";
  if (!tokenData) {
    signupClass = "d-flex justify-content-center mb-2";
    eventClass = "d-none d-flex justify-content-center mb-2";
  }
  return (
    <>
      <section>
        <MDBContainer className="py-5">
          {/* <MDBRow></MDBRow> */}

          <MDBRow>
            <MDBCol lg="4" style={{ marginTop: "45px" }}>
              <MDBCard className="mb-4">
                <MDBCardBody className="text-center">
                  <MDBCardImage
                    src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                    alt="avatar"
                    className="rounded-circle"
                    style={{ width: "150px" }}
                    fluid
                  />
                  <h2 className="mb-1" style={{ color: "black" }}>
                    {upperFirstChar(bartenderData?.user.first) +
                      " " +
                      upperFirstChar(bartenderData?.user.last)}
                  </h2>
                  <p>{upperFirstChar(bartenderData?.user.city)}, TX</p>
                  <p>{bartenderData?.about} </p>
                  <p>Contact me at: {bartenderData?.user.email} </p>
                  <div className={signupClass}>Sign up to book today!</div>
                  <div className={eventClass}>
                    <BookEventModal id={bartenderData?.id} />
                  </div>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
            <MDBCol lg="8">
              <MDBRow>
                <MDBCol md="6">
                  <MDBCard
                    className="mb-4 mb-md-0"
                    style={{ marginTop: "45px" }}
                  >
                    <MDBCardBody>
                      {/* <MDBCardText className="mb-4"> */}
                      {/* <span className="text-primary text-center font-italic me-1">
                          Reviews
                        </span>{" "} */}
                      <table className="table table-striped text-center">
                        <thead>
                          <tr>
                            <th>Specialty Drinks</th>
                          </tr>
                        </thead>
                        <tbody>
                          {drinksObjArray?.map((drink) => {
                            return (
                              <tr key={drink.name}>
                                <td>{drink.name}</td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                      {/* </MDBCardText> */}
                    </MDBCardBody>
                  </MDBCard>
                </MDBCol>

                <MDBCol md="6">
                  {/* <MDBCard
                    // className="mb-4 mb-md-0"
                    style={{ backgroundColor: "rgba(0,0,0,0)" }}
                  > */}
                  <Carousel interval={1500} fade>
                    {drinksObjArray?.map((drink, index) => {
                      return (
                        <Carousel.Item key={index}>
                          <MDBCardImage
                            className="d-block w-100"
                            src={drink.image}
                            alt={drink.name}
                            style={{
                              width: "500px",
                              height: "500px",
                              objectFit: "contain",
                            }}
                          />
                          <Carousel.Caption>
                            <h3 className="text-outline">{drink.name}</h3>
                          </Carousel.Caption>
                        </Carousel.Item>
                      );
                    })}
                  </Carousel>
                  {/* </MDBCard> */}
                </MDBCol>
              </MDBRow>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>
    </>
  );
}

export default BartenderDetail;

import React, { useState, useEffect } from "react";
import { useGetBartendersQuery } from "../store/bartendersApi";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import BartenderDetail from "./bartenderDetail";

function BartenderList() {
  const { data: bartendersData, isLoading } = useGetBartendersQuery();
  const [filteredResults, setFilteredResults] = useState([]);
  const [bartender, setId] = useState({ id: null });
  const [searchInput, setSearchInput] = useState("");

  const changeId = (bartender) => {
    setId({ id: bartender.id });
  };
  const resetId = () => {
    setId({ id: null });
  };

  function upperFirstChar(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  useEffect(() => {
    if (searchInput) {
      let filteredData = bartendersData.bartenders?.filter((bartender) =>
        bartender.user.city.includes(searchInput.toLowerCase())
      );
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(bartendersData?.bartenders);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchInput]);

  useEffect(() => {
    if (bartendersData) {
      setFilteredResults(bartendersData?.bartenders);
    }
  }, [bartendersData]);

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  if (bartender.id !== null) {
    return (
      <>
        <Button
          style={{
            marginLeft: "1em",
            marginTop: "10px",
            backgroundColor: "#f8f3e9",
            color: "black",
            border: "none",
            fontSize: "14px",
          }}
          onClick={resetId}
        >
          Go back
        </Button>
        <BartenderDetail id={bartender.id} />
      </>
    );
  }
  return (
    <div className="bartenderList" style={{ height: "auto", width: "auto" }}>
      {/* The className is adding the extra space above search by city */}
      <header>
        <div className="container position-relative">
          <div className="row justify-content-center">
            <div className="col-xl-6">
              <div className="mb-6 text-center text-white">
                {/* <!-- Page heading--> */}
                <h1
                  className="mb-6 content1 text-outline"
                  style={{
                    color: "#f8f3e9",
                    textDecoration: "outline",
                    textDecorationColor: "black",
                  }}
                >
                  Where will your event be held?
                </h1>
                <br />
                <form
                  className="form-subscribe"
                  data-sb-form-api-token="API_TOKEN"
                >
                  {/* <!-- Email address input--> */}
                  <div className="row">
                    <div className="col">
                      <input
                        icon="search"
                        placeholder="City"
                        onChange={(e) => setSearchInput(e.target.value)}
                        type="text"
                        className="form-control form-control-md"
                        // style={{
                        //   width: "22rem",
                        //   margin: "0 50%",
                        // }}
                      ></input>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </header>
      <br />
      {/* this next div contains all the new logic, stopping at the export */}
      <div className="container" style={{ height: "1080px" }}>
        <div
          className="row"
          style={{
            margin: "0",
            display: "flex",
            justifyContent: "center",
          }}
        >
          {searchInput.length >= 1
            ? filteredResults.map((bartender) => {
                return (
                  <Card
                    className="col-md-3 shadow"
                    style={{
                      margin: "10px",
                      display: "flex",
                      justifyContent: "center",
                      textAlign: "center",
                    }}
                    key={bartender.id}
                  >
                    <Card.Img
                      variant="top"
                      src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                      alt="avatar"
                      className="rounded-circle"
                      style={{
                        width: "150px",
                        margin: "10px auto",
                        display: "block",
                      }}
                    />
                    <Card.Body>
                      <Card.Title>
                        {upperFirstChar(bartender.user.first) +
                          " " +
                          upperFirstChar(bartender.user.last)}
                      </Card.Title>
                      <Card.Text>
                        {bartender.user.city.charAt(0).toUpperCase() +
                          bartender.user.city.slice(1)}
                      </Card.Text>
                    </Card.Body>{" "}
                    <Button
                      style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "center",
                      }}
                      onClick={() => {
                        changeId(bartender);
                      }}
                      variant="primary"
                    >
                      View Profile
                    </Button>
                    <br />
                  </Card>
                );
              })
            : bartendersData.bartenders?.map((bartender) => {
                return (
                  <Card
                    className="col-md-3 shadow"
                    key={bartender.id}
                    style={{
                      margin: "10px",
                      display: "flex",
                      justifyContent: "center",
                      textAlign: "center",
                    }}
                  >
                    <Card.Img
                      variant="top"
                      src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                      alt="avatar"
                      className="rounded-circle text-center"
                      style={{
                        width: "150px",
                        margin: "10px auto",
                        display: "block",
                      }}
                    />
                    {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
                    <Card.Body>
                      <Card.Title>
                        {upperFirstChar(bartender.user.first) +
                          " " +
                          upperFirstChar(bartender.user.last)}
                      </Card.Title>
                      <Card.Text>
                        {bartender.user.city.charAt(0).toUpperCase() +
                          bartender.user.city.slice(1)}
                        , {bartender.user.state}
                      </Card.Text>
                    </Card.Body>
                    <Button
                      style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "center",
                      }}
                      onClick={() => {
                        changeId(bartender);
                      }}
                      variant="primary"
                    >
                      View Profile
                    </Button>
                    <br />
                  </Card>
                );
              })}
        </div>
      </div>
    </div>
  );
}
export default BartenderList;

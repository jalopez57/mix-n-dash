import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import BulmaInput from "./bulmaInput";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useCreateUserMutation } from "../store/accountsApi";
import { useLoginMutation, useGetTokenQuery } from "../store/accountsApi";

function UserSignupModal() {
  const { data: tokenData } = useGetTokenQuery();
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [error, setError] = useState("");
  const navigate = useNavigate();
  const [signup, result] = useCreateUserMutation();
  const [first, setFirst] = useState("");
  const [last, setLast] = useState("");
  const [phoneNumber, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [state, setState] = useState("TX");
  const [city, setCity] = useState("");
  const [login] = useLoginMutation();

  async function handleSubmit(e) {
    e.preventDefault();
    signup({ first, last, phoneNumber, email, password, state, city });
  }
  useEffect(() => {
    if (result.isSuccess) {
      handleClose();
      login({ email, password });
      setFirst("");
      setLast("");
      setPhone("");
      setEmail("");
      setPassword("");
      setCity("");
      setState("TX");
      navigate("/account/detail");
    } else if (result.isError) {
      setError(result.error);
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result]);

  let primary = "primary";
  if (tokenData) {
    primary = "primary d-none";
  }

  return (
    <>
      <Button
        style={{ color: "#F4EBD0" }}
        className="dropdown-item nav-item"
        variant={primary}
        onClick={handleShow}
      >
        Sign up
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New user</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit}>
            <BulmaInput
              label="First"
              id="first"
              placeholder="first name"
              value={first}
              onChange={setFirst}
            />
            <BulmaInput
              label="Last"
              id="last"
              placeholder="last name"
              value={last}
              onChange={setLast}
            />
            <BulmaInput
              label="Phone Number"
              id="phoneNumber"
              placeholder="phone number"
              value={phoneNumber}
              onChange={setPhone}
            />
            <BulmaInput
              label="Email"
              id="email"
              placeholder="email"
              value={email}
              onChange={setEmail}
              type="email"
            />
            <BulmaInput
              label="Password"
              id="password"
              placeholder="password"
              value={password}
              onChange={setPassword}
              type="password"
            />
            <BulmaInput
              label="City"
              id="city"
              placeholder="city"
              value={city}
              onChange={setCity}
            />
            <div className="field">
              <button className="btn btn-primary">Create account</button>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default UserSignupModal;

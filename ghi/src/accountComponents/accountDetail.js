import { useState } from "react";
import { useGetUserQuery, useGetTokenQuery } from "../store/accountsApi";
import { useGetBartendersQuery } from "../store/bartendersApi";
import { useGetEventsQuery } from "../store/eventsApi";
import DeleteEventModal from "./eventCancelModal";
import ErrorNotification from "../ErrorNotification";
import UpdateUserModal from "./accountUpdateModal";
import UpdateBartenderModal from "../bartenderComponents/bartenderUpdateModal";
import UserSignupModal from "./accountSignupModal";
import BartenderSignupModal from "../bartenderComponents/bartenderSignupModal";
import BartenderDetail from "../bartenderComponents/bartenderDetail";
import Button from "react-bootstrap/Button";

// Card styles
import {
  MDBCol,
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardText,
  MDBCardBody,
  MDBCardImage,
  MDBTableHead,
  MDBTableBody,
} from "mdb-react-ui-kit";
import LoginModal from "./loginModal";

function UserDetail() {
  const { data: tokenData } = useGetTokenQuery();
  const { data: userData, error, isLoading } = useGetUserQuery();
  const { data: bartenderData } = useGetBartendersQuery();
  const { data: eventsData } = useGetEventsQuery();
  const [bartender, setId] = useState({ id: null });
  let bartenderProfile = "d-none";
  let noBartender = "d-flex justify-content-center mb-2";
  let hasBartender = "d-flex justify-content-center mb-2 d-none";
  let hasUserEvents = "mb-4 d-none";
  let hasBartenderEvents = "mb-4 d-none";

  const userBartender = bartenderData?.bartenders?.find(function (bartender) {
    return bartender.user.id === userData?.id;
  });
  const userEvents = eventsData?.events.filter(function (event) {
    return event.user.id === userData?.id;
  });
  const bartenderEvents = eventsData?.events.filter(function (event) {
    return event.bartender.id === userBartender?.id;
  });
  const changeId = (bartender) => {
    setId({ id: bartender.id });
  };
  const resetId = () => {
    setId({ id: null });
  };

  function upperFirstChar(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function formatTime(timeString) {
    const [hourString, minute] = timeString.split(":");
    const hour = +hourString % 24;
    return (hour % 12 || 12) + ":" + minute + (hour < 12 ? "AM" : "PM");
  }

  function formatDate(dateString) {
    const date = dateString;
    const [year, month, day] = date.split("-");
    const result = [month, day, year].join("/");
    return result;
  }

  if (userBartender) {
    bartenderProfile = "";
    hasBartender = "d-flex justify-content-center mb-2";
    noBartender = "d-flex justify-content-center mb-2 d-none";
  }

  if (userEvents?.length > 0) {
    hasUserEvents = "mb-4";
  }

  if (bartenderEvents?.length > 0) {
    hasBartenderEvents = "mb-4";
  }

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  if (!tokenData) {
    return (
      <div
        style={{
          padding: "15%",
          margin: "auto",
          width: "100%",
        }}
      >
        <h2 style={{ color: "#f8f3e9" }}>
          Create an account or sign in to use this page
        </h2>
        <UserSignupModal />
        <LoginModal />
      </div>
    );
  }

  if (bartender.id !== null) {
    return (
      <>
        <Button
          style={{
            marginLeft: "1em",
            marginTop: "10px",
            backgroundColor: "#f8f3e9",
            color: "black",
            border: "none",
          }}
          onClick={resetId}
        >
          Back to account
        </Button>
        <BartenderDetail id={bartender.id} />
      </>
    );
  }

  return (
    <>
      <div className="columns is-centered">
        <div className="column is-narrow">
          <ErrorNotification error={error} />
        </div>
      </div>
      <section>
        <MDBContainer>
          <MDBRow>
            <MDBCol lg="4 mt-4 mb-4">
              <MDBCard className="mb-4">
                <MDBCardBody className="text-center">
                  <MDBCardImage
                    src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                    alt="avatar"
                    className="rounded-circle"
                    style={{ width: "150px" }}
                    fluid
                  />
                  <h2>{userData.first + " " + userData.last}</h2>
                  <div className={noBartender}>
                    <UpdateUserModal />
                    <BartenderSignupModal />
                  </div>
                  <div className={hasBartender}>
                    <UpdateUserModal />
                    <UpdateBartenderModal />
                  </div>
                </MDBCardBody>
                <table className="table text-center">
                  <tbody>
                    <tr>
                      <td>
                        <b>Phone Number:</b> {userData.phone_number}{" "}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <b>Email:</b> {userData.email}{" "}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <b>Location:</b> {upperFirstChar(userData.city)},{" "}
                        {userData.state}{" "}
                      </td>
                    </tr>
                  </tbody>
                </table>
                <Button
                  className={bartenderProfile}
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                  onClick={() => {
                    changeId(userBartender);
                  }}
                  variant="primary"
                >
                  View Bartender Profile
                </Button>
              </MDBCard>
              {/* <MDBCard className="mb-4 mb-lg-0">
                <MDBCardBody className="p-0">
                  <MDBTable className="table table-striped text-center">
                    <MDBTableHead>
                      <tr>
                        <th>Specialty Drinks</th>
                      </tr>
                    </MDBTableHead>
                    <MDBTableBody>
                      {bartenderDrinks?.map((drink) => {
                        return (
                          <tr key={drink}>
                            <td>{drink}</td>
                          </tr>
                        );
                      })}
                    </MDBTableBody>
                  </MDBTable>
                </MDBCardBody>
              </MDBCard> */}
            </MDBCol>
            <MDBCol lg="8 mb-4 mt-4">
              <MDBCard className={hasUserEvents}>
                <MDBCardBody>
                  <MDBCardText className="mb-4">
                    <span className="text-primary font-italic me-1">
                      Upcoming Events
                    </span>{" "}
                  </MDBCardText>
                  <table className="table is-striped">
                    <MDBTableHead>
                      <tr>
                        <th style={{ textAlign: "left" }}>Address</th>
                        <th style={{ textAlign: "left" }}>City</th>
                        <th style={{ textAlign: "left" }}>State</th>
                        <th style={{ textAlign: "left" }}>Date</th>
                        <th style={{ textAlign: "left" }}>Time</th>
                        <th style={{ textAlign: "left" }}>Bartender</th>
                      </tr>
                    </MDBTableHead>
                    <MDBTableBody>
                      {userEvents?.map((event) => (
                        <tr key={event.id}>
                          <td
                            style={{
                              verticalAlign: "middle",
                              textAlign: "left",
                            }}
                          >
                            {event.address}
                          </td>
                          <td
                            style={{
                              verticalAlign: "middle",
                              textAlign: "left",
                            }}
                          >
                            {event.city}
                          </td>
                          <td
                            style={{
                              verticalAlign: "middle",
                              textAlign: "left",
                            }}
                          >
                            {event.state}
                          </td>
                          <td
                            style={{
                              verticalAlign: "middle",
                              textAlign: "left",
                            }}
                          >
                            {formatDate(event.date)}
                          </td>
                          <td
                            style={{
                              verticalAlign: "middle",
                              textAlign: "left",
                            }}
                          >
                            {formatTime(event.time)}
                          </td>
                          <td
                            style={{
                              verticalAlign: "middle",
                              textAlign: "left",
                            }}
                          >
                            {upperFirstChar(
                              event.bartender.bartender_info.first
                            ) +
                              " " +
                              upperFirstChar(
                                event.bartender.bartender_info.last
                              )}
                          </td>
                          <td>
                            <DeleteEventModal eventId={event.id} />
                          </td>
                        </tr>
                      ))}
                    </MDBTableBody>
                  </table>
                </MDBCardBody>
              </MDBCard>
              <MDBCard className={hasBartenderEvents}>
                <MDBCardBody>
                  <MDBRow lg="6">
                    <MDBCardBody>
                      <MDBCardText className="mb-4">
                        <span className="text-primary font-italic me-1">
                          Bartender Bookings
                        </span>{" "}
                      </MDBCardText>
                      <table className="table is-striped">
                        <MDBTableHead>
                          <tr>
                            <th style={{ textAlign: "left" }}>Address</th>
                            <th style={{ textAlign: "left" }}>City</th>
                            <th style={{ textAlign: "left" }}>State</th>
                            <th style={{ textAlign: "left" }}>Date</th>
                            <th style={{ textAlign: "left" }}>Time</th>
                            <th style={{ textAlign: "left" }}>Host Name</th>
                          </tr>
                        </MDBTableHead>
                        <MDBTableBody>
                          {bartenderEvents?.map((event) => (
                            <tr key={event.id}>
                              <td
                                style={{
                                  verticalAlign: "middle",
                                  textAlign: "left",
                                }}
                              >
                                {event.address}
                              </td>
                              <td
                                style={{
                                  verticalAlign: "middle",
                                  textAlign: "left",
                                }}
                              >
                                {event.city}
                              </td>
                              <td
                                style={{
                                  verticalAlign: "middle",
                                  textAlign: "left",
                                }}
                              >
                                {event.state}
                              </td>
                              <td
                                style={{
                                  verticalAlign: "middle",
                                  textAlign: "left",
                                }}
                              >
                                {formatDate(event.date)}
                              </td>
                              <td
                                style={{
                                  verticalAlign: "middle",
                                  textAlign: "left",
                                }}
                              >
                                {formatTime(event.time)}
                              </td>
                              <td
                                style={{
                                  verticalAlign: "middle",
                                  textAlign: "left",
                                }}
                              >
                                {upperFirstChar(event.user.first) +
                                  " " +
                                  upperFirstChar(event.user.last)}
                              </td>
                            </tr>
                          ))}
                        </MDBTableBody>
                      </table>
                    </MDBCardBody>
                  </MDBRow>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>
    </>
  );
}
export default UserDetail;

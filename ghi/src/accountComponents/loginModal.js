import "../css/index.css";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useLoginMutation, useGetTokenQuery } from "../store/accountsApi";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import BulmaInput from "./bulmaInput";
import ErrorNotification from "../ErrorNotification";

function LoginModal() {
  const { data: tokenData } = useGetTokenQuery();
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const navigate = useNavigate();
  const [login, result] = useLoginMutation();
  const [password, setPassword] = useState("");
  const [email, setUsername] = useState("");
  const [error, setError] = useState("");

  async function handleSubmit(e) {
    e.preventDefault();
    login({ email, password });
    setPassword("");
    setUsername("");
  }

  useEffect(() => {
    if (result.isSuccess) {
      setError("");
      handleClose();
      navigate("/account/detail");
    } else if (result.isError) {
      setError(result.error.data.detail);
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result]);

  let primary = "primary";
  if (tokenData) {
    primary = "primary d-none";
  }
  return (
    <>
      <Button
        style={{ color: "#F4EBD0" }}
        className="dropdown-item nav-item"
        variant={primary}
        onClick={handleShow}
      >
        Login
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <ErrorNotification error={error} />
            <form className="auth-inner" onSubmit={handleSubmit}>
              <BulmaInput
                label="Username"
                id="username"
                placeholder="username"
                value={email}
                onChange={setUsername}
              />
              <BulmaInput
                label="Password"
                id="password"
                placeholder="password"
                value={password}
                onChange={setPassword}
                type="password"
              />
              <div className="d-grid">
                <button className="btn btn-primary">Login</button>
              </div>
            </form>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default LoginModal;

import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import BulmaInput from "./bulmaInput";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useCreateEventMutation } from "../store/eventsApi";

function BookEventModal(bartenderId) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [error, setError] = useState("");
  const navigate = useNavigate();
  const [book, result] = useCreateEventMutation();
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [bartender, setBartender] = useState(null);
  async function handleSubmit(e) {
    e.preventDefault();
    book({ address, city, state, date, time, bartender });
  }
  useEffect(() => {
    if (result.isSuccess) {
      handleClose();
      setAddress("");
      setCity("");
      setState("");
      setDate("");
      setTime("");
      navigate("/account/detail");
    } else if (result.isError) {
      setError(result.error);
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result]);

  return (
    <>
      <Button
        variant="primary"
        onClick={() => {
          setBartender(bartenderId.id);
          handleShow();
        }}
      >
        Book this bartender
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Book an event</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit}>
            <BulmaInput
              label="Address"
              id="address"
              placeholder="address"
              value={address}
              onChange={setAddress}
            />
            <BulmaInput
              label="City"
              id="city"
              placeholder="city"
              value={city}
              onChange={setCity}
            />
            <BulmaInput
              label="Date"
              id="date"
              placeholder="date"
              value={date}
              onChange={setDate}
              type="date"
            />
            <BulmaInput
              label="Time"
              id="time"
              placeholder="time"
              value={time}
              onChange={setTime}
              type="time"
            />
            <div className="field">
              <button type="submit" className="btn btn-primary">
                Book now
              </button>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default BookEventModal;

import "../css/index.css";
import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import BulmaInput from "./bulmaInput";
import { useNavigate } from "react-router-dom";
import { useGetUserQuery, useUpdateUserMutation } from "../store/accountsApi";

function UpdateUserModal() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const { data, isLoading } = useGetUserQuery();
  const [error, setError] = useState("");
  const navigate = useNavigate();
  const [update, result] = useUpdateUserMutation();
  const [first, setFirst] = useState("");
  const [last, setLast] = useState("");
  const [phoneNumber, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [state, setState] = useState("TX");
  const [city, setCity] = useState(upperFirstChar(""));

  async function handleSubmit(e) {
    e.preventDefault();
    update({ first, last, phoneNumber, email, password, state, city });
    setFirst(first);
    setLast(last);
    setPhone(phoneNumber);
    setEmail(email);
    setPassword("");
    setCity(upperFirstChar(city));
    setState(state);
  }

  function upperFirstChar(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function handleClick() {
    handleShow();
    setFirst(data.first);
    setLast(data.last);
    setPhone(data.phone_number);
    setEmail(data.email);
    setPassword("");
    setCity(upperFirstChar(data.city));
  }
  useEffect(() => {
    if (result.isSuccess) {
      handleClose();
      navigate("/account/detail");
    } else if (result.isError) {
      setError(result.error);
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result]);

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  return (
    <>
      <Button
        variant="primary"
        onClick={handleClick}
        style={{ fontSize: "14px" }}
      >
        Update Account Info
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Account</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleSubmit}>
            <BulmaInput
              label="First"
              id="first"
              required={false}
              placeholder={data.first}
              value={first}
              onChange={setFirst}
            />
            <BulmaInput
              label="Last"
              id="last"
              placeholder={data.last}
              value={last}
              onChange={setLast}
            />
            <BulmaInput
              label="Phone Number"
              id="phoneNumber"
              placeholder={data.phone_number}
              value={phoneNumber}
              onChange={setPhone}
            />
            <BulmaInput
              label="Email"
              id="email"
              placeholder={data.email}
              value={email}
              onChange={setEmail}
            />
            <BulmaInput
              label="Password"
              id="password"
              placeholder="password"
              value={password}
              onChange={setPassword}
            />
            {/* <BulmaInput
              label="State"
              id="state"
              placeholder={data.state}
              value={state}
              onChange={setState}
            /> */}
            <BulmaInput
              label="City"
              id="city"
              placeholder={data.city}
              value={city}
              onChange={setCity}
            />
            <div className="field">
              <button className="btn btn-primary">Update</button>
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer></Modal.Footer>
      </Modal>
    </>
  );
}

export default UpdateUserModal;

import "../css/index.css";

function BulmaInput(props) {
  const {
    type = "text",
    required = true,
    label,
    id,
    placeholder,
    value,
    onChange,
  } = props;

  return (
    <div className="mb-3">
      <label htmlFor={id}>{label}</label>
      <input
        required={required}
        value={value}
        onChange={(e) => onChange(e.target.value)}
        type={type}
        className="form-control"
        placeholder={placeholder}
        id={id}
      />
    </div>
  );
}

export default BulmaInput;

import json
from fastapi.testclient import TestClient
from main import app
from queries.bartenders import BartenderQueries
from routers.auth import authenticator

client = TestClient(app)  # replacing swagger in code


class BartenderQueriesMock:
    def get_all_bartenders(self):
        return []

    def create_bartender(self, bartender, user_id):
        response = {
            "id": 156,
            "about": "I'm awesome",
            "drinks": ["drink1", "drink2"],
            "user": {
                "id": user_id,
                "first": "Nick",
                "last": "Doutrich",
                "phone_number": "1111111",
                "email": "nick.doutrich@gmail.com",
                "state": "TX",
                "city": "Dallas",
            },
        }
        response.update(bartender)
        return response


def test_list_bartenders():
    # arrange
    app.dependency_overrides[BartenderQueries] = BartenderQueriesMock
    # act
    response = client.get("/api/bartenders/")

    # assert
    # 1. get a 200
    # 2. should call queries.get_all_bartenders()
    assert response.status_code == 200
    assert response.json() == {"bartenders": []}

    # cleanup
    app.dependency_overrides = {}


def test_create_bartender():
    app.dependency_overrides[BartenderQueries] = BartenderQueriesMock
    user = {"id": 112, "email": "nick.doutrich@gmail.com"}
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = lambda: user
    bartender = {
        "about": "I am awesome",
        "drinks": ["Drink1", "Drink2"],
    }
    response = client.post("/api/bartenders/", json.dumps(bartender))

    assert response.status_code == 200
    assert response.json()["about"] == "I am awesome"
    assert response.json()["user"]["id"] == 112

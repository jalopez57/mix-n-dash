import json
from fastapi.testclient import TestClient
from main import app
from queries.users import UserQueries

client = TestClient(app)  # replacing swagger in code


class UserQueriesMock:
    def get_all_users(self):
        return []

    def create_user(self, user, hashed_password):
        response = {}
        response.update(user)
        return response


def test_list_users():
    # arrange
    app.dependency_overrides[UserQueries] = UserQueriesMock

    # act
    response = client.get("/api/users/")

    # assert
    # 1. get a 200
    # 2. should call queries.get_all_users()
    assert response.status_code == 200
    assert response.json() == {"users": []}

    # cleanup
    app.dependency_overrides = {}

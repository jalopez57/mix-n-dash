from fastapi import APIRouter, Depends, HTTPException, status
from .auth import authenticator
from queries.events import EventQueries
from models import EventIn, EventOut, EventUpdate, EventsOut


router = APIRouter()


@router.get("/api/events/", response_model=EventsOut)
def events_list(queries: EventQueries = Depends()):
    return {
        "events": queries.get_all_events(),
    }


@router.post("/api/events/", response_model=EventOut)
async def create_event(
    event_in: EventIn,
    queries: EventQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.create_event(event_in, account_data.get("id"))


@router.get("/api/events/{event_id}", response_model=EventOut)
def get_event(
    event_id: int,
    queries: EventQueries = Depends(),
):
    record = queries.get_event(event_id)
    if record is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Event does not exist",
        )
    else:
        return record


@router.put("/api/events/{event_id}", response_model=EventUpdate)
def update_event(
    event_id: int,
    event_in: EventUpdate,
    queries: EventQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.update_event(event_id, event_in, account_data.get("id"))


@router.delete("/api/events/{event_id}", response_model=bool)
def delete_event(
    event_id: int,
    queries: EventQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    queries.delete_event(event_id, account_data.get("id"))
    return True

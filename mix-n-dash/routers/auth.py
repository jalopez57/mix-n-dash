import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.users import UserQueries
from models import UserOut, UserIn


class Auth(Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts: UserQueries,
    ) -> UserIn:
        return accounts.get_user(username)

    def get_account_getter(
        self, accounts: UserQueries = Depends()
    ) -> UserQueries:
        return accounts

    def get_hashed_password(self, account: UserIn):
        return account.get("password")

    def get_account_data_for_cookie(self, user: UserIn) -> UserOut:
        return user.get("email"), UserOut(**user)


authenticator = Auth(os.environ["SIGNING_KEY"])

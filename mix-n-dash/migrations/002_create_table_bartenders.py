steps = [
    [
        """
        CREATE TABLE bartenders (
            id SERIAL NOT NULL UNIQUE,
            about TEXT NOT NULL,
            drinks TEXT [] NOT NULL,
            user_id INTEGER NOT NULL UNIQUE REFERENCES users("id") ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE bartenders;
        """
    ]
]

steps = [
    [
        """
        CREATE TABLE users (
            id SERIAL NOT NULL UNIQUE,
            first TEXT NOT NULL,
            last TEXT NOT NULL,
            phone_number TEXT NOT NULL,
            email TEXT NOT NULL UNIQUE,
            password TEXT NOT NULL,
            state TEXT NOT NULL,
            city TEXT NOT NULL
        );
        """,
        """
        DROP TABLE users;
        """
    ]
]

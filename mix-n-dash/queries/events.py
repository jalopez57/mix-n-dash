import os
from psycopg_pool import ConnectionPool
from queries.bartenders import BartenderQueries
from routers.bartenders import get_bartender
from fastapi import Response

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class EventQueries:
    def get_all_events(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT u.id AS user_id, u.first, u.last, u.phone_number,
                    u.email, u.city, u.state,
                        b.id AS bartender_id, b.about, b.drinks,
                        e.id AS event_id, e.address, e.city, e.state, e.date,
                        e.time
                    FROM events e
                    INNER JOIN bartenders b ON b.id = e.bartender_id
                    INNER JOIN users u ON u.id = e.user_id

                    GROUP BY u.id, u.first, u.last, u.phone_number,
                    u.email, u.city, u.state,
                        b.id, b.about, b.drinks, b.user_id,
                        e.id, e.address, e.city, e.state, e.date, e.time

                    ORDER BY e.id
                """
                )

                results = []
                rows = cur.fetchall()
                for row in rows:
                    event = self.event_record_to_dict(row, cur.description)
                    results.append(event)
                return results

    def get_event(self, event_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT u.id AS user_id, u.first, u.last, u.phone_number,
                    u.email, u.city, u.state,
                        b.id AS bartender_id, b.about, b.drinks,
                        e.id AS event_id, e.address, e.city, e.state, e.date,
                        e.time
                    FROM events e
                    INNER JOIN bartenders b ON b.id = e.bartender_id
                    INNER JOIN users u ON u.id = e.user_id
                    WHERE e.id = %s
                """,
                    [event_id],
                )
                row = cur.fetchone()
                return self.event_record_to_dict(row, cur.description)

    def create_event(self, data, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.address,
                    data.city,
                    data.state,
                    data.date,
                    data.time,
                    user_id,
                    data.bartender_id,
                ]
                cur.execute(
                    """
                    INSERT INTO events (address, city, state, date, time,
                    user_id, bartender_id)
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING id, address, city, state, date, time, user_id,
                    bartender_id
                    """,
                    params,
                )

                row = cur.fetchone()
                id = row[0]
        if id is not None:
            return self.get_event(id)

    def update_event(self, event_id, data, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.address,
                    data.city,
                    data.state,
                    data.date,
                    data.time,
                    user_id,
                    event_id,
                ]
                cur.execute(
                    """
                    UPDATE events
                    SET address = %s, city = %s, state = %s, date = %s,
                    time = %s
                    WHERE user_id = %s and id = %s
                    RETURNING id, address, city, state, date, time
                    """,
                    params,
                )

                row = cur.fetchone()
                id = row[0]
        if id is not None:
            return self.get_event(id)

    def delete_event(self, event_id, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM events
                    WHERE id = %s and user_id = %s
                    """,
                    [event_id, user_id],
                )

    def event_record_to_dict(self, row, description):
        event = None
        if row is not None:
            event = {}
            event_fields = [
                "event_id",
                "address",
                "city",
                "state",
                "date",
                "time",
            ]
            for i, column in enumerate(description):
                if column.name in event_fields:
                    event[column.name] = row[i]
            event["id"] = event["event_id"]

            bartender = {}
            bartender_fields = [
                "bartender_id",
                "about",
                "drinks",
                "bartender_info",
            ]
            for i, column in enumerate(description):
                if column.name in bartender_fields:
                    bartender[column.name] = row[i]
            bartender["id"] = bartender["bartender_id"]

            bartender_info = {}
            bartender_data = get_bartender(
                bartender_id=bartender["id"],
                response=Response,
                queries=BartenderQueries(),
            )

            bartender_info["first"] = bartender_data.get("user").get("first")
            bartender_info["last"] = bartender_data.get("user").get("last")
            bartender_info["phone_number"] = bartender_data.get("user").get(
                "phone_number"
            )
            bartender_info["email"] = bartender_data.get("user").get("email")

            bartender["bartender_info"] = bartender_info
            event["bartender"] = bartender

            user = {}
            user_fields = [
                "user_id",
                "first",
                "last",
                "phone_number",
                "email",
                "city",
                "state",
            ]
            for i, column in enumerate(description):
                if column.name in user_fields:
                    user[column.name] = row[i]
            user["id"] = user["user_id"]

            event["user"] = user
        return event
